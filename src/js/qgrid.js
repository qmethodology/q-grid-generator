/** QGrid class with base methods */
class QGrid{
  
  /**
  * @constructor
  * @param {number=} card_num_opt - Number of cards
  * @param {number=} scale_opt - Grid scale range (±)
  */
  constructor(cards_num_opt = 0, scale_opt = 0){
    this.grid = [];
    this.cards_num = cards_num_opt;
    this.scale = scale_opt;
    this.columns = 2*this.scale + 1;
  }

  /** Compare arrays a1 & a2 for equality 
  * @param {Array} a1 - First array
  * @param {Array} a1 - Second array
  * @return {boolean}
  * @protected
  */
  arraysEqual_(a1, a2) {
    if(a1.length !== a2.length){
      return false;
    }
    for(var i = a1.length; i--;) {
      if(a1[i] !== a2[i]){
        return false;
      }
    }
    return true;
  }

  /** Complement grid from its half 
  * @return {Array<number>}
  * @protected
  */
  complement_(){
    let grid_half = this.grid.slice();
    return this.grid = grid_half.slice(1,grid_half.length).reverse().concat(grid_half);
  }

  /** Draw grid in HTML to "render-area" DIV 
  * return {boolean}
  * @protected
  */
  render_(){
    if(this.grid){
      const grid = this.grid.slice();
      const grid_center = (grid.length-1)/2;
      let html = '<div class="grid">'
      html += `<div class="grid__graphical">`;
      for(let x = 0; x < grid.length; x++){
        let column = '<div class="grid__column">';
        for (let i = 1; i <= grid[x]; i++){
          column += '<div class="grid__block"></div>';
        }
        const scale_index = -(grid_center) + x;
        column += `<div class="grid__number">${scale_index}</div>`;
        column +='</div>';
        html += column;
      }
      html += '</div>';
      html += `<div class="grid__array">[${grid.toString()}]</div>`;
      html += '</div>';
      document.getElementById('render-area').innerHTML = html;
    } else{
      document.getElementById('render-area').innerHTML = '<div class="grid"><div class="grid__error">Not enought blocks to build the grid!</div></div>';
    }

    return true;
  }

  /** Setter of grid attributes
  * @param {number} card_num_arg - Number of cards
  * @param {number} scale_arg - Grid scale range (±)
  * @param {number} flatten_ratio_arg - Flatten ratio to flatten/sharpen the distribution
  * @return {boolean}
  * @public
  */
  set_attributes(cards_num_arg, scale_arg, flatten_ratio_arg = 1){
    this.cards_num = Number(cards_num_arg) || this.cards_num;
    this.scale = Number(scale_arg) || this.scale;
    this.columns = 2*this.scale + 1;
    this.flatten_ratio = Number(flatten_ratio_arg)
    return true;
  }

  /** Get grid as an array 
  * @return {Array<number>}
  * @public
  */
  get_grid(){
    return this.grid.slice();
  }

  /** Get grid as a string 
  * @return {string}
  * @public
  */
  get_grid_str(){
    return this.grid.toString();
  }

  /** Print grid to console
  * @return {boolean}
  * @public
  */
  print_grid(){
    console.log(this.grid);
    return true;
  }
}

//___________________________________________________________________
//

/** QGridLinear class - grid with linear distribution */
class QGridLinear extends QGrid {
  
   /**
  * @constructor
  * @param {number} card_num_arg - Number of cards
  * @param {number} scale_arg - Grid scale range (±)
  */
  constructor(cards_num_arg, scale_arg){
    super(cards_num_arg, scale_arg);
  }

  /**
  * Initialize grid - check values for validity & fill each column with one card 
  * @return {boolean}
  * @protected
  **/
  initialize_(){
    if(this.cards_num < this.columns){
      this. grid = false;
    } else{
      let grid = [];
      for(let i=0; i < this.columns; i++){
        grid.push(1)
      }
      grid[this.scale] += this.cards_num - this.columns;
      this.grid = grid.slice(this.scale);
    }

    return true;
  }

  /**
  * Kick down two blocks from center to edge
  * @param {boolean=} perform_kick_opt - Perform actual kick on the grid
  * @protected
  * @return {Array<number>}
  */
  kick_down_(perform_kick_opt = true){
    let grid = this.grid.slice();
    
    const diff = 1;
    const kick_condition = (grid[0]-2 > grid[1]);
    const len = grid.length;
    
    if(kick_condition){
      grid[0] -= 2;
      let change = false;
      for(let x = 1; x < len; x++){
        if( (grid[len-x-1] - grid[len-x]) > diff ){
          grid[len-x]++;
          change = true;
          break;
        }
      }
      if(!change){
        grid[0] += 2;
      }
    }

    if(perform_kick_opt){
      this.grid = grid.slice();
    }

    return grid.slice();
  }

  /**
  * Build the grid
  * @param {number} card_num_arg - Number of cards
  * @param {number} scale_arg - Grid scale range (±)
  * @public
  * @return {Array<number>}
  */
  build(cards_num_arg, scale_arg){
    this.set_attributes(cards_num_arg, scale_arg);
    this.initialize_();

    if(this.grid){
      while(!this.arraysEqual_(this.grid, this.kick_down_())){
        this.kick_down_(true);
      }
      this.complement_();
    }
  
    this.render_()

    return this.grid;
  }
}

//___________________________________________________________________
//

/** QGridNormal class - grid with normal distribution */
class QGridNormal extends QGrid {
  
  /**
  * @constructor
  * @param {number} card_num_arg - Number of cards
  * @param {number} scale_arg - Grid scale range (±)
  */
  constructor(cards_num_arg, scale_arg, flatten_ratio_arg){
    super(cards_num_arg, scale_arg);
    this.flatten_ratio = Number(flatten_ratio_arg);
  }

  /**
  * Treat a possibility of one missing/redundant block
  * @protected
  * @return {Boolean}
  **/
  finalize_(){
    let grid = this.grid.slice();
    // Count blocks in grid
    let sum = 0;
    for (let i = 0; i <= this.scale; i++) {
      if (i == 0) {
        sum += grid[i];
      } else {
        sum += grid[i] * 2;
      }
    }
    // Find redundant/missing block
    const remainder = this.blocks - sum;
    // Take care of it
    let grid_final = grid.slice();
    if (grid_final[0] + remainder >= grid[1]) {
      grid_final[0] += remainder;
    } else {
      for (let i = grid_final.length - 1; i > 0; i--) {
        if (grid_final[i] > 1) {
          grid_final[i]--;
          grid_final[0]++;
          break;
        }
      }
    }

    this.grid = grid_final;

    return true;
  }

  /**
  * Make sure the grid will be always non decresing from its edge to the center
  * @protected
  * @return {boolean}
  */
  iron_(grid){
    var ironed_grid = grid.slice();
    const len = ironed_grid.length;
    // If the non descending rule is broken, shift the blocks to the center
    for (let i = len - 1; i > 0; i--) {
      if (ironed_grid[i] > ironed_grid[i - 1]) {
        // Take the blocks breaking the rule
        let diff = ironed_grid[i] - ironed_grid[i - 1];
        ironed_grid[i] -= diff;
        // Shift them to the center
        if (i != 1) {
          ironed_grid[i - 1] += diff;
        } else {
          ironed_grid[0] += 2 * diff;
        }
      }
    }

    return  ironed_grid;
  }

  /**
  * Build the grid
  * @param {number} card_num_arg - Number of cards
  * @param {number} scale_arg - Grid scale range (±)
  * @param {number} flatten_ration_arg - Grid scale range (±)
  * @public
  */
  build(cards_num_arg, scale_arg, flatten_ratio_arg){

    this.set_attributes(cards_num_arg, scale_arg, flatten_ratio_arg);
    // Standard normal ditribution
    const distribution = gaussian(0, 1);
    // Every column will have at least one block, therefore subtract them from the pack of blocks to be distributed
    const blocks_to_distribute = this.cards_num - this.columns;
    // Distribution scales from 0 to 3 * standard deviation, which gives 3 for normal distribution 
    const step = 3 / this.scale;
    // Initialize variables
    let prob_sum = 0;
    let probs = [];
    let prob;
    // Calculate distribution for each scale value
    for (let i = 0; i <= this.scale; i++) {
      // Distribution value
      prob = distribution.pdf(i * step);
      // Value multiplied by flatten ratio
      prob = Math.pow(prob, this.flatten_ratio);
      //Save the value
      probs.push(prob);
      if (i == 0) {
        prob_sum += prob;
      } else {
        prob_sum += 2 * prob;
      }
    }
    
    // Initialize variables
    let block_prob = prob_sum / blocks_to_distribute;
    let grid = [];
    let error = 0;
    let blocks_col = void 0;
    let blocks_col_round = void 0;
    // Calculate number of blocks for each scale value respective to its distribution value
    for (let i = 0; i <= this.scale; i++) {
      
      // Calculate number of blocks
      blocks_col = probs[i] / block_prob + error;
      console.log(blocks_col);
      blocks_col_round = Math.round(blocks_col);
      // Get the rounding error
      error = blocks_col - blocks_col_round;
      // Save the blocks to grid
      grid.push(blocks_col_round + 1);
      if (i == 0) {
        error = error / 2;
      }
      if (i > 0) {
        //Check the non descending rule
        grid = this.iron_(grid);
      }
    }
    this.grid = grid;
    console.log('--',grid,'--')

    // Finalize grid
    this.finalize_();
  
    // Complement grid
    this.complement_()

    this.print_grid();

    // Render the grid
    this.render_();

    return this.grid;
  }
}