const gridLinear = new QGridLinear();
const gridNormal = new QGridNormal();

$("#render_linear").click((e) => {
  $("#form_linear").submit();
});

$("#render_normal").click((e) => {
  $("#form_normal").submit();
});

$("#form_linear").submit((e) => {
  e.preventDefault();
  const cards_num = $("#form_linear_cards_num").val();
  const scale = $("#form_linear_scale").val();
  gridLinear.build(cards_num, scale);
});

$("#form_normal").submit((e) => {
  e.preventDefault();
  const cards_num = $("#form_normal_cards_num").val();
  const scale = $("#form_normal_scale").val();
  const flatten_ratio = $("#form_normal_flatten_ratio").val();
  //console.log(cards_num, scale, flatten_ratio);
  gridNormal.build(cards_num, scale, flatten_ratio);
});


