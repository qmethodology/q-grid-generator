'use strict';

/*****
** Compare two arrays for equality
*****/
function arraysEqual(a1, a2) {
  if (a1.length !== a2.length) {
    return false;
  }
  for (var i = a1.length; i--;) {
    if (a1[i] !== a2[i]) {
      return false;
    }
  }
  return true;
}

/*****
** Initialize grid
*****/
function initialize_grid(blocks_count_str, scale_str) {
  var blocks_count = Number(blocks_count_str);
  var scale = Number(scale_str);
  var cols = scale * 2 + 1;
  if (blocks_count < cols) {

    return false;
  } else {
    var grid = [];
    for (var i = 0; i < cols; i++) {
      grid.push(1);
    }
    grid[scale] += blocks_count - cols;

    return grid.slice(scale);
  }
}

/*****
** Kick down two blocks from center to edge
*****/
function kick_down(original_grid, equal_columns) {
  var grid = original_grid.slice();
  var diff = equal_columns ? 0 : 1;
  var kick_condition = equal_columns ? grid[0] - 2 >= grid[1] : grid[0] - 2 > grid[1];
  var len = grid.length;

  if (kick_condition) {
    grid[0] -= 2;
    var change = false;
    for (var x = 1; x < len; x++) {
      if (grid[len - x - 1] - grid[len - x] > diff) {
        grid[len - x]++;
        change = true;
        break;
      }
    }
    if (!change) {
      grid[0] += 2;
    }
  }

  return grid;
}

/*****
** Complement the grid
*****/
function complement_grid(pyramid_half) {
  return pyramid_half.slice(1, pyramid_half.length).reverse().concat(pyramid_half);
}

/*****
** Construct grid
*****/
function build_grid() {
  var blocks_number = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
  var scale = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var equal_columns = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

  var grid = initialize_grid(blocks_number, scale);

  if (grid) {
    while (!arraysEqual(grid, kick_down(grid, equal_columns))) {
      grid = kick_down(grid, equal_columns);
    }

    return complement_grid(grid);
  } else {

    return false;
  }
}

/*****
** Draw grid in HTML
*****/
function draw_grid_html(grid) {
  if (grid) {
    var grid_center = (grid.length - 1) / 2;
    var html = '<div class="grid">';
    html += '<div class="info">[' + grid.toString() + '] = ' + grid.reduce(function (a, b) {
      return a + b;
    }) + ' blocks</div>';
    html += '<div class="grid">';
    for (var x = 0; x < grid.length; x++) {
      var column = '<div class="col">';
      for (var i = 1; i <= grid[x]; i++) {
        column += '<div class="block"></div>';
      }
      var scale_index = -grid_center + x;
      column += '<div class="number">' + scale_index + '</div>';
      column += '</div>';
      html += column;
    }
    html += '</div></div>';
    document.getElementById('result').innerHTML = html;

    return html;
  } else {
    document.getElementById('result').innerHTML = '<div class="error">Not enought blocks to build the grid!</div>';
  }
}