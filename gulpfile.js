var gulp = require('gulp');

var babel = require("gulp-babel");
var watch = require('gulp-watch');
var sass = require('gulp-sass');

gulp.task("babel", function () {
  return gulp.src("./src/js/*.js")
    .pipe(babel())
    .pipe(gulp.dest("./dist/js/"));
});

gulp.task('sass', function () {
  return gulp.src('./src/scss/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./dist/css/'));
});

gulp.task("watch", function (){
  gulp.watch(["./src/scss/*.scss"], ['sass']);
  gulp.watch(["./src/js/*.js"], ['babel']);
});

// Default Task
gulp.task('default', ['babel', 'sass', 'watch']);