'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/** QGrid class with base methods */
var QGrid = function () {

  /**
  * @constructor
  * @param {number=} card_num_opt - Number of cards
  * @param {number=} scale_opt - Grid scale range (±)
  */
  function QGrid() {
    var cards_num_opt = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
    var scale_opt = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

    _classCallCheck(this, QGrid);

    this.grid = [];
    this.cards_num = cards_num_opt;
    this.scale = scale_opt;
    this.columns = 2 * this.scale + 1;
  }

  /** Compare arrays a1 & a2 for equality 
  * @param {Array} a1 - First array
  * @param {Array} a1 - Second array
  * @return {boolean}
  * @protected
  */


  _createClass(QGrid, [{
    key: 'arraysEqual_',
    value: function arraysEqual_(a1, a2) {
      if (a1.length !== a2.length) {
        return false;
      }
      for (var i = a1.length; i--;) {
        if (a1[i] !== a2[i]) {
          return false;
        }
      }
      return true;
    }

    /** Complement grid from its half 
    * @return {Array<number>}
    * @protected
    */

  }, {
    key: 'complement_',
    value: function complement_() {
      var grid_half = this.grid.slice();
      return this.grid = grid_half.slice(1, grid_half.length).reverse().concat(grid_half);
    }

    /** Draw grid in HTML to "render-area" DIV 
    * return {boolean}
    * @protected
    */

  }, {
    key: 'render_',
    value: function render_() {
      if (this.grid) {
        var grid = this.grid.slice();
        var grid_center = (grid.length - 1) / 2;
        var html = '<div class="grid">';
        html += '<div class="grid__graphical">';
        for (var x = 0; x < grid.length; x++) {
          var column = '<div class="grid__column">';
          for (var i = 1; i <= grid[x]; i++) {
            column += '<div class="grid__block"></div>';
          }
          var scale_index = -grid_center + x;
          column += '<div class="grid__number">' + scale_index + '</div>';
          column += '</div>';
          html += column;
        }
        html += '</div>';
        html += '<div class="grid__array">[' + grid.toString() + ']</div>';
        html += '</div>';
        document.getElementById('render-area').innerHTML = html;
      } else {
        document.getElementById('render-area').innerHTML = '<div class="grid"><div class="grid__error">Not enought blocks to build the grid!</div></div>';
      }

      return true;
    }

    /** Setter of grid attributes
    * @param {number} card_num_arg - Number of cards
    * @param {number} scale_arg - Grid scale range (±)
    * @param {number} flatten_ratio_arg - Flatten ratio to flatten/sharpen the distribution
    * @return {boolean}
    * @public
    */

  }, {
    key: 'set_attributes',
    value: function set_attributes(cards_num_arg, scale_arg) {
      var flatten_ratio_arg = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;

      this.cards_num = Number(cards_num_arg) || this.cards_num;
      this.scale = Number(scale_arg) || this.scale;
      this.columns = 2 * this.scale + 1;
      this.flatten_ratio = Number(flatten_ratio_arg);
      return true;
    }

    /** Get grid as an array 
    * @return {Array<number>}
    * @public
    */

  }, {
    key: 'get_grid',
    value: function get_grid() {
      return this.grid.slice();
    }

    /** Get grid as a string 
    * @return {string}
    * @public
    */

  }, {
    key: 'get_grid_str',
    value: function get_grid_str() {
      return this.grid.toString();
    }

    /** Print grid to console
    * @return {boolean}
    * @public
    */

  }, {
    key: 'print_grid',
    value: function print_grid() {
      console.log(this.grid);
      return true;
    }
  }]);

  return QGrid;
}();

//___________________________________________________________________
//

/** QGridLinear class - grid with linear distribution */


var QGridLinear = function (_QGrid) {
  _inherits(QGridLinear, _QGrid);

  /**
  * @constructor
  * @param {number} card_num_arg - Number of cards
  * @param {number} scale_arg - Grid scale range (±)
  */
  function QGridLinear(cards_num_arg, scale_arg) {
    _classCallCheck(this, QGridLinear);

    return _possibleConstructorReturn(this, (QGridLinear.__proto__ || Object.getPrototypeOf(QGridLinear)).call(this, cards_num_arg, scale_arg));
  }

  /**
  * Initialize grid - check values for validity & fill each column with one card 
  * @return {boolean}
  * @protected
  **/


  _createClass(QGridLinear, [{
    key: 'initialize_',
    value: function initialize_() {
      if (this.cards_num < this.columns) {
        this.grid = false;
      } else {
        var grid = [];
        for (var i = 0; i < this.columns; i++) {
          grid.push(1);
        }
        grid[this.scale] += this.cards_num - this.columns;
        this.grid = grid.slice(this.scale);
      }

      return true;
    }

    /**
    * Kick down two blocks from center to edge
    * @param {boolean=} perform_kick_opt - Perform actual kick on the grid
    * @protected
    * @return {Array<number>}
    */

  }, {
    key: 'kick_down_',
    value: function kick_down_() {
      var perform_kick_opt = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

      var grid = this.grid.slice();

      var diff = 1;
      var kick_condition = grid[0] - 2 > grid[1];
      var len = grid.length;

      if (kick_condition) {
        grid[0] -= 2;
        var change = false;
        for (var x = 1; x < len; x++) {
          if (grid[len - x - 1] - grid[len - x] > diff) {
            grid[len - x]++;
            change = true;
            break;
          }
        }
        if (!change) {
          grid[0] += 2;
        }
      }

      if (perform_kick_opt) {
        this.grid = grid.slice();
      }

      return grid.slice();
    }

    /**
    * Build the grid
    * @param {number} card_num_arg - Number of cards
    * @param {number} scale_arg - Grid scale range (±)
    * @public
    * @return {Array<number>}
    */

  }, {
    key: 'build',
    value: function build(cards_num_arg, scale_arg) {
      this.set_attributes(cards_num_arg, scale_arg);
      this.initialize_();

      if (this.grid) {
        while (!this.arraysEqual_(this.grid, this.kick_down_())) {
          this.kick_down_(true);
        }
        this.complement_();
      }

      this.render_();

      return this.grid;
    }
  }]);

  return QGridLinear;
}(QGrid);

//___________________________________________________________________
//

/** QGridNormal class - grid with normal distribution */


var QGridNormal = function (_QGrid2) {
  _inherits(QGridNormal, _QGrid2);

  /**
  * @constructor
  * @param {number} card_num_arg - Number of cards
  * @param {number} scale_arg - Grid scale range (±)
  */
  function QGridNormal(cards_num_arg, scale_arg, flatten_ratio_arg) {
    _classCallCheck(this, QGridNormal);

    var _this2 = _possibleConstructorReturn(this, (QGridNormal.__proto__ || Object.getPrototypeOf(QGridNormal)).call(this, cards_num_arg, scale_arg));

    _this2.flatten_ratio = Number(flatten_ratio_arg);
    return _this2;
  }

  /**
  * Treat a possibility of one missing/redundant block
  * @protected
  * @return {Boolean}
  **/


  _createClass(QGridNormal, [{
    key: 'finalize_',
    value: function finalize_() {
      var grid = this.grid.slice();
      // Count blocks in grid
      var sum = 0;
      for (var i = 0; i <= this.scale; i++) {
        if (i == 0) {
          sum += grid[i];
        } else {
          sum += grid[i] * 2;
        }
      }
      // Find redundant/missing block
      var remainder = this.blocks - sum;
      // Take care of it
      var grid_final = grid.slice();
      if (grid_final[0] + remainder >= grid[1]) {
        grid_final[0] += remainder;
      } else {
        for (var _i = grid_final.length - 1; _i > 0; _i--) {
          if (grid_final[_i] > 1) {
            grid_final[_i]--;
            grid_final[0]++;
            break;
          }
        }
      }

      this.grid = grid_final;

      return true;
    }

    /**
    * Make sure the grid will be always non decresing from its edge to the center
    * @protected
    * @return {boolean}
    */

  }, {
    key: 'iron_',
    value: function iron_(grid) {
      var ironed_grid = grid.slice();
      var len = ironed_grid.length;
      // If the non descending rule is broken, shift the blocks to the center
      for (var i = len - 1; i > 0; i--) {
        if (ironed_grid[i] > ironed_grid[i - 1]) {
          // Take the blocks breaking the rule
          var diff = ironed_grid[i] - ironed_grid[i - 1];
          ironed_grid[i] -= diff;
          // Shift them to the center
          if (i != 1) {
            ironed_grid[i - 1] += diff;
          } else {
            ironed_grid[0] += 2 * diff;
          }
        }
      }

      return ironed_grid;
    }

    /**
    * Build the grid
    * @param {number} card_num_arg - Number of cards
    * @param {number} scale_arg - Grid scale range (±)
    * @param {number} flatten_ration_arg - Grid scale range (±)
    * @public
    */

  }, {
    key: 'build',
    value: function build(cards_num_arg, scale_arg, flatten_ratio_arg) {

      this.set_attributes(cards_num_arg, scale_arg, flatten_ratio_arg);
      // Standard normal ditribution
      var distribution = gaussian(0, 1);
      // Every column will have at least one block, therefore subtract them from the pack of blocks to be distributed
      var blocks_to_distribute = this.cards_num - this.columns;
      // Distribution scales from 0 to 3 * standard deviation, which gives 3 for normal distribution 
      var step = 3 / this.scale;
      // Initialize variables
      var prob_sum = 0;
      var probs = [];
      var prob = void 0;
      // Calculate distribution for each scale value
      for (var i = 0; i <= this.scale; i++) {
        // Distribution value
        prob = distribution.pdf(i * step);
        // Value multiplied by flatten ratio
        prob = Math.pow(prob, this.flatten_ratio);
        //Save the value
        probs.push(prob);
        if (i == 0) {
          prob_sum += prob;
        } else {
          prob_sum += 2 * prob;
        }
      }

      // Initialize variables
      var block_prob = prob_sum / blocks_to_distribute;
      var grid = [];
      var error = 0;
      var blocks_col = void 0;
      var blocks_col_round = void 0;
      // Calculate number of blocks for each scale value respective to its distribution value
      for (var _i2 = 0; _i2 <= this.scale; _i2++) {

        // Calculate number of blocks
        blocks_col = probs[_i2] / block_prob + error;
        console.log(blocks_col);
        blocks_col_round = Math.round(blocks_col);
        // Get the rounding error
        error = blocks_col - blocks_col_round;
        // Save the blocks to grid
        grid.push(blocks_col_round + 1);
        if (_i2 == 0) {
          error = error / 2;
        }
        if (_i2 > 0) {
          //Check the non descending rule
          grid = this.iron_(grid);
        }
      }
      this.grid = grid;
      console.log('--', grid, '--');

      // Finalize grid
      this.finalize_();

      // Complement grid
      this.complement_();

      this.print_grid();

      // Render the grid
      this.render_();

      return this.grid;
    }
  }]);

  return QGridNormal;
}(QGrid);