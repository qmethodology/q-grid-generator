"use strict";

var gridLinear = new QGridLinear();
var gridNormal = new QGridNormal();

$("#render_linear").click(function (e) {
  $("#form_linear").submit();
});

$("#render_normal").click(function (e) {
  $("#form_normal").submit();
});

$("#form_linear").submit(function (e) {
  e.preventDefault();
  var cards_num = $("#form_linear_cards_num").val();
  var scale = $("#form_linear_scale").val();
  gridLinear.build(cards_num, scale);
});

$("#form_normal").submit(function (e) {
  e.preventDefault();
  var cards_num = $("#form_normal_cards_num").val();
  var scale = $("#form_normal_scale").val();
  var flatten_ratio = $("#form_normal_flatten_ratio").val();
  //console.log(cards_num, scale, flatten_ratio);
  gridNormal.build(cards_num, scale, flatten_ratio);
});